package dfs_applications;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import graph.Graph;
import graph.Graph.Vertex;


public class DFS  {
	
	// Djupet först
	public static <V,E> void dfs(Graph<V,E> g) {
		g.unvisit();
		for (Graph.Vertex<V,E> v : g) {
			if (!v.isVisited()) {
				dfs(v);
			}
		}
	}
	// Visits all nodes connected to node param v. 
	private static <V,E> void dfs(Graph.Vertex<V,E> v) {
		v.visit();
		for (Graph.Edge<V,E> e : v) {
			Graph.Vertex<V,E> w = e.destination();
			if (!w.isVisited()) {
				dfs(w);
			}
		}
	}
	
	public static <V,E> boolean isConnected(Graph<V,E> g) {
		g.unvisit();
		dfs(g.iterator().next());
		for(Graph.Vertex<V,E> v : g) if(!v.isVisited()) return false;
		return true;
	}
	
	// Om grafen är connected == 1 component
	// Gå igenom noder, gör inget om besökt
	// Annars traversera alla noder som är nåbara från noden v
	
	public static <V,E> int nbrOfComponents(Graph<V,E> g) {
		int components;
		if(isConnected(g)) return 1; // Components is 1
		else {
			components = 1; // we have established it's non-connected
			for(Graph.Vertex<V,E> v : g) { 
				if (v.isVisited()) continue; // Om den redan är besökt, hoppa över
				else {
					dfs(v);
					components++;
				}
			}
		g.unvisit();
		}
		return components;
	}
	
	public static <V,E> boolean pathExists(Graph<V,E> g,
			Graph.Vertex<V,E> v, Graph.Vertex<V,E> u)
	{	
		boolean pathFound = false;
		if(isConnected(g)) return true; // graph connected, path exists
		g.unvisit();
		for(Graph.Edge<V, E> e : v)
		{
			if(e.destination() == u) return true; // if v->u by connection
			pathFound =  pathExistsRec(e.destination(), u);
		}
		return pathFound;
	}
	
	
	private static <V,E> boolean pathExistsRec(Graph.Vertex<V,E> ver,
			Graph.Vertex<V,E> target) 
	{
		if(!ver.isVisited()) {
			ver.visit();
			// Visits all nodes connected to node param v. 
			for (Graph.Edge<V,E> e : ver) {
				if(e.destination() == target) return true;
				else return pathExistsRec(e.destination(), target);
			}
		}
		return false;

	}
 
	public static <V,E> List<Graph.Vertex<V,E>> findPath(Graph<V,E> g,
			Graph.Vertex<V,E> v, Graph.Vertex<V,E> u) 
	{
		g.unvisit();
		List<Graph.Vertex<V,E>> path = new ArrayList<Vertex<V, E>>();
		path.add(v);
		if(findPath(path, v, u) == false) {
			path.clear();
		}
		return path;
	}
	
	private static <V, E> boolean findPath(List<Graph.Vertex<V, E>> l,
			Graph.Vertex<V,E> v, Graph.Vertex<V,E> target) 
	{
		Graph.Vertex<V, E> w = v; // vertex object
		if(!v.isVisited()) { 
			v.visit(); // we have visited this node
			for(Graph.Edge<V, E> e : v)
			{
				w = e.destination(); // vertex obj = destination of edge from vertex 
				if(w == target) {
					l.add(w); // If target found, add and return true, to break our of the recursion
					return true; // if v->u by connection
				}
				else {
					l.add(w); // target hasnt been found but vertex was visited, then added to list
					return findPath(l, w, target); // rec call itsel
					}
				}
			}
		l.remove(w); // If vertex is completely not in the path, remove from the list
		return false;
	}


	
}

