package dfs_applications;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import graph.Graph;
import graph.Graph.Edge;
import graph.Graph.Vertex;


public class DFS  {
	
	// Djupet först
	public static <V,E> void dfs(Graph<V,E> g) {
		g.unvisit();
		for (Graph.Vertex<V,E> v : g) { // for every vertex in g
			if (!v.isVisited()) {
				dfs(v);
			}
		}
	}
	// Visits all nodes connected to node param v. 
	private static <V,E> void dfs(Graph.Vertex<V,E> v) {
		v.visit();
		for (Graph.Edge<V,E> e : v) { // for every edge in g
			Graph.Vertex<V,E> w = e.destination(); // w = other end of edge
			if (!w.isVisited()) {
				dfs(w); // do it all again with different verticies
			}
		}
	}
	
	public static <V,E> boolean isConnected(Graph<V,E> g) {
		g.unvisit();
		dfs(g.iterator().next());
		for(Graph.Vertex<V,E> v : g) if(!v.isVisited()) return false;
		return true;
	}
	
	// Om grafen är connected == 1 component
	// Gå igenom noder, gör inget om besökt
	// Annars traversera alla noder som är nåbara från noden v
	
	public static <V,E> int nbrOfComponents(Graph<V,E> g) {
		int components;
		if(isConnected(g)) return 1; // Components is 1
		else {
			components = 1; // we have established it's non-connected
			for(Graph.Vertex<V,E> v : g) { 
				if (v.isVisited()) continue; // Om den redan är besökt, hoppa över
				else {
					dfs(v);
					components++;
				}
			}
		g.unvisit();
		}
		return components;
	}
	
	public static <V,E> boolean pathExists(Graph<V,E> g,
			Graph.Vertex<V,E> v, Graph.Vertex<V,E> u)
	{	
		boolean pathFound = false;
		if(isConnected(g)) return true; // graph connected, path exists
		g.unvisit();
		for(Graph.Edge<V, E> e : v)
		{
			if(e.destination() == u) return true; // if v->u by connection
			pathFound =  pathExistsRec(e.destination(), u);
		}
		return pathFound;
	}
	
	
	private static <V,E> boolean pathExistsRec(Graph.Vertex<V,E> ver,
			Graph.Vertex<V,E> target) 
	{
		if(!ver.isVisited()) {
			ver.visit();
			// Visits all nodes connected to node param v. 
			for (Graph.Edge<V,E> e : ver) {
				if(e.destination() == target) return true;
				else return pathExistsRec(e.destination(), target);
			}
		}
		return false;

	}
 
	public static <V,E> List<Graph.Vertex<V,E>> findPath(Graph<V,E> g,
			Graph.Vertex<V,E> v, Graph.Vertex<V,E> u) 
	{
			g.unvisit();
			List<Graph.Vertex<V,E>> path = new ArrayList<Vertex<V, E>>();
			path.add(v);
			v.visit();
			if(v == u) return path;	// start == end
			for(Graph.Edge<V, E> edge : v) {
				if(findPath(path, edge.destination(), u)) {
					return path;
				}
			}
			path.clear();
			return path;
		}
	
	private static <V, E> boolean findPath(List<Graph.Vertex<V, E>> l,
			Graph.Vertex<V,E> v, Graph.Vertex<V,E> target) 
	{
		if(v == target) {
			l.add(target);
			return true;
		}
		if(!v.isVisited()) {
			l.add(v);
			v.visit();
			for (Graph.Edge<V, E> edge : v) {
				if(edge.destination() == target) {
					l.add(edge.destination());
					return true; 
					} 
				else {
					if(!edge.destination().isVisited())
					return findPath(l, edge.destination(), target);
				}
			}
		}
			l.remove(v);
		return false;
	}	
}