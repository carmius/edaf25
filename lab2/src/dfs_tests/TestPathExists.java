package dfs_tests;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Random;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import digraph.DiGraph;
import graph.Graph;
import graph.Graph.Vertex;
import dfs_applications.DFS;


public class TestPathExists {
	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public final void testOneVertex() {
		Graph<String,Integer> g = 
			new DiGraph<String,Integer>();
		Graph.Vertex<String,Integer> v = g.addVertex("a");
		assertTrue(DFS.pathExists(g,v,v));
	}
	
	@Test
	public final void testTwoVerticesNoPath() {
		Graph<String,Integer> g = 
			new DiGraph<String,Integer>();
		Graph.Vertex<String,Integer> v1 = g.addVertex("a");
		Graph.Vertex<String,Integer> v2 =g.addVertex("b");
		assertFalse(DFS.pathExists(g,v1,v2));
	}
	
	@Test
	public final void testTwoVerticesPathExists() {
		Graph<String,Integer> g = 
			new DiGraph<String,Integer>();
		Graph.Vertex<String,Integer> v1 = g.addVertex("a");
		Graph.Vertex<String,Integer> v2 =g.addVertex("b");
		g.addEdge(1,v1,v2);
		g.addEdge(1,v2,v1);
		assertTrue(DFS.pathExists(g,v1,v2));
		assertTrue(DFS.pathExists(g,v2,v1));
	}
	
	@Test
	public final void testThreeVerticesPathsExist() {
		Graph<String,Integer> g = 
			new DiGraph<String,Integer>();
		Graph.Vertex<String,Integer> v1 = g.addVertex("a");
		Graph.Vertex<String,Integer> v2 =g.addVertex("b");
		Graph.Vertex<String,Integer> v3 =g.addVertex("c");
		g.addEdge(1,v1,v2);
		g.addEdge(1,v2,v1);
		g.addEdge(1,v1,v3);
		g.addEdge(1,v3,v1);
		assertTrue(DFS.pathExists(g,v1,v2));
		assertTrue(DFS.pathExists(g,v2,v3));
		assertTrue(DFS.pathExists(g,v3,v1));
	}
	
	@Test
	public final void testThreeVerticesNoPath() {
		Graph<String,Integer> g = 
			new DiGraph<String,Integer>();
		Graph.Vertex<String,Integer> v1 = g.addVertex("a");
		Graph.Vertex<String,Integer> v2 =g.addVertex("b");
		Graph.Vertex<String,Integer> v3 =g.addVertex("c");
		g.addEdge(1,v2,v3);
		g.addEdge(1,v3,v2);
		assertFalse(DFS.pathExists(g,v1,v2));
		assertFalse(DFS.pathExists(g,v1,v3));
	}
	
	@Test
	public final void testFiveVerticesPathsExist() {
		Graph<String,Integer> g = 
			new DiGraph<String,Integer>();
		Graph.Vertex<String,Integer> v1 = g.addVertex("a");
		Graph.Vertex<String,Integer> v2 =g.addVertex("b");
		Graph.Vertex<String,Integer> v3 =g.addVertex("c");
		Graph.Vertex<String,Integer> v4 =g.addVertex("d");
		Graph.Vertex<String,Integer> v5 =g.addVertex("e");
		g.addEdge(1,v1,v2);
		g.addEdge(1,v2,v1);
		g.addEdge(1,v1,v3);
		g.addEdge(1,v3,v1);
		g.addEdge(1,v2,v3);
		g.addEdge(1,v3,v2);
		g.addEdge(1,v1,v4);
		g.addEdge(1,v4,v1);
		g.addEdge(1,v4,v5);
		g.addEdge(1,v5,v4);
		assertTrue(DFS.pathExists(g,v1,v2));
		assertTrue(DFS.pathExists(g,v2,v3));
		assertTrue(DFS.pathExists(g,v3,v4));
		assertTrue(DFS.pathExists(g,v4,v5));
		assertTrue(DFS.pathExists(g,v5,v1));
	}
	
	@Test
	public final void testFiveVerticesTwoComponents() {
		Graph<String,Integer> g = 
			new DiGraph<String,Integer>();
		Graph.Vertex<String,Integer> v1 = g.addVertex("a");
		Graph.Vertex<String,Integer> v2 =g.addVertex("b");
		Graph.Vertex<String,Integer> v3 =g.addVertex("c");
		Graph.Vertex<String,Integer> v4 =g.addVertex("d");
		Graph.Vertex<String,Integer> v5 =g.addVertex("e");
		g.addEdge(1,v1,v2);
		g.addEdge(1,v2,v1);
		g.addEdge(1,v1,v3);
		g.addEdge(1,v3,v1);
		g.addEdge(1,v2,v3);
		g.addEdge(1,v3,v2);
		g.addEdge(1,v5,v4);
		g.addEdge(1,v4,v5);
		assertFalse(DFS.pathExists(g,v1,v4));
		assertFalse(DFS.pathExists(g,v2,v4));
		assertFalse(DFS.pathExists(g,v3,v4));
		assertFalse(DFS.pathExists(g,v5,v1));
		assertFalse(DFS.pathExists(g,v5,v2));
		assertFalse(DFS.pathExists(g,v5,v3));
	}
	@Test
	public final void testFiveVerticesFiveComponents2() {
		Graph<String,Integer> g = 
			new DiGraph<String,Integer>();
		Graph.Vertex<String,Integer> v1 = g.addVertex("a");
		Graph.Vertex<String,Integer> v2 =g.addVertex("b");
		Graph.Vertex<String,Integer> v3 =g.addVertex("c");
		Graph.Vertex<String,Integer> v4 =g.addVertex("d");
		Graph.Vertex<String,Integer> v5 =g.addVertex("e");
		assertFalse(DFS.pathExists(g,v1,v2));
		assertFalse(DFS.pathExists(g,v2,v3));
		assertFalse(DFS.pathExists(g,v3,v4));
		assertFalse(DFS.pathExists(g,v4,v5));
		assertFalse(DFS.pathExists(g,v5,v1));
		assertTrue(!DFS.pathExists(g,v5,v1));
	}
	@Test
	public final void testMapQuest() {
		Random r = new Random(203040340L);
		Graph<String,Integer> g = 
			new DiGraph<String,Integer>();
		Graph.Vertex<String,Integer> v1 = g.addVertex("1");
		Graph.Vertex<String,Integer> v2 = g.addVertex("2");
		Graph.Vertex<String,Integer> v3 = g.addVertex("3");
		Graph.Vertex<String,Integer> v4 = g.addVertex("4");
		Graph.Vertex<String,Integer> v5 = g.addVertex("5");
		Graph.Vertex<String,Integer> v6 = g.addVertex("6");
		Graph.Vertex<String,Integer> v7 = g.addVertex("7");
		Graph.Vertex<String,Integer> v8 = g.addVertex("8");
		Graph.Vertex<String,Integer> v9 = g.addVertex("9");
		Graph.Vertex<String,Integer> v10 = g.addVertex("10");
		Graph.Vertex<String,Integer> v11 = g.addVertex("Target");
		Graph.Vertex<String,Integer> v12 = g.addVertex("12");
		g.addEdge(10, v1, v2);
		g.addEdge(20, v1, v3);
		g.addEdge(30, v3, v4);
		g.addEdge(40, v2, v6);
		g.addEdge(10, v4, v5);
		g.addEdge(10, v5, v6);
		g.addEdge(20, v6, v7);
		g.addEdge(30, v7, v8);
		g.addEdge(40, v8, v10);
		g.addEdge(10, v8, v9);
		g.addEdge(20, v9, v10);
		g.addEdge(30, v10, v11);
		g.addEdge(30, v1, v12);
		
		ArrayList<Graph.Vertex<String, Integer>> l = (ArrayList<Vertex<String, Integer>>)DFS.findPath(g, v1, v11);
		
		System.out.println("If path was found; here one of those are:\n" );
		for(Graph.Vertex<String, Integer> e : l) {
			System.out.println(e.toString());
		}
		
		
	}


}
