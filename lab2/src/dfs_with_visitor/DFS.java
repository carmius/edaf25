package dfs_with_visitor;
import graph.Graph;


public class DFS {
	

	public  static <V,E> void dfs(Graph.Vertex<V,E> v, VertexVisitor<V,E> visitor) 
	{
		if (visitor.isDone()) {
			return;
		}
		visitor.preVisit(v);
		v.visit();
		for (Graph.Edge<V,E> e : v) {
			Graph.Vertex<V,E> w = e.destination();
			if (!w.isVisited()) {
				dfs(w,visitor);
			}
		}
		visitor.postVisit(v);
		
		
	}
	public static <V,E> boolean isConnected(Graph<V,E> g)
	{
		g.unvisit();
		DFS.dfs(g.iterator().next(), new VertexVisitor<V,E>());
		for (Graph.Vertex<V,E> v : g) {
			if (!v.isVisited()) {
				return false;
				}
			}
			return true;
		}
}
