package dfs_with_visitor;
import graph.Graph;

public class VertexVisitor<V, E> {
	public boolean isDone() {
		return false;
	}
	
	public void preVisit(Graph.Vertex<V,E> v) {}
	
	public void postVisit(Graph.Vertex<V,E> v) {}
	
	public static <V,E> boolean isConnected(Graph<V,E> g) 
	{
		g.unvisit();
		DFS.dfs(g.iterator().next(), new VertexVisitor<V,E>());
		for (Graph.Vertex<V,E> v : g) {
			if (!v.isVisited()) return false;
		}
		return true;
	}
	
	public static <V, E> boolean pathExists(Graph<V, E> g, 
			Graph.Vertex<V, E> v, Graph.Vertex<V, E> u)
	{
		return false;
	}
	
	/*
	 * 	public static <V,E> boolean pathExists(Graph<V,E> g,
			Graph.Vertex<V,E> v, Graph.Vertex<V,E> u)
	{	
		boolean pathFound = false;
		if(isConnected(g)) return true; // graph connected, path exists
		g.unvisit();
		for(Graph.Edge<V, E> e : v)
		{
			if(e.destination() == u) return true; // if v->u by connection
			pathFound =  pathExistsRec(e.destination(), u);
		}
		return pathFound;
	}
	 * 
	 * 
	 */
	
	
	public static <V,E> int componentSize(Graph<V,E> g,
			Graph.Vertex<V,E> v)
	{
			CountingVisitor<V,E> cv= new CountingVisitor<V,E>();
			g.unvisit();
			DFS.dfs(v,cv);
			return cv.count;
	}
	
	static class CountingVisitor<V,E> extends VertexVisitor<V,E> 
	{
		private int count;
		public CountingVisitor() {
			super();
			count = 0;
		}
		public void preVisit(Graph.Vertex<V,E> v) 
		{
			count++;
		}
	}
}
