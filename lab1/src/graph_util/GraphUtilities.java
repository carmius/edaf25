package graph_util;
import graph.Graph;
import graph.Graph.Vertex;

import java.util.HashSet;
import java.util.Set;
// Test
public class GraphUtilities {
	public static <V,E> int nbrOfVertices(Graph<V,E> g) {
		int vertices = 0;
		int test;
		for(Graph.Vertex<V, E> v : g) {
			if(!v.isVisited()) {
				vertices++;
				v.visit();
			}
		}
		g.unvisit();
		return vertices;
	}
	
	
	public static <V,E> int nbrOfEdges(Graph<V,E> g, boolean directed) {
		//*** EGET SKRIVET ***
		int edges = 0;
		for(Graph.Vertex<V, E> v : g) {
			for(Graph.Edge<V, E> e : v) {
				edges++;
			}
		}
		System.out.println(edges);
		if(!directed) edges /= 2;
		return edges;
		// ********
	}
	
	public static <V,E> boolean edgeBetween(Graph.Vertex<V,E> from, 
										Graph.Vertex<V,E> to) {
		// Gå igenom alla edge objekt i vertex-objeket och jämför med to
		for(Graph.Edge<V, E> e : from) {
			if(e.destination() == to) return true;
		}
		// vice versa
		for(Graph.Edge<V, E> e : to) {
			if(e.destination() == from) return true;
		}
		return false;
	}
}
